# MiniStockMarket infrastructure
Create namespace for app:
```bash
kubectl apply -f Namespace.yaml
```
Configure ClusterRole and ClusterRoleBinding for namespace:
```bash
kubectl apply -f Roles.yaml
```
Deploy self signed tls certificate using instructions from `ssl/README.md`