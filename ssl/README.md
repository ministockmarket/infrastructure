# OpenSSL Certificate generation

Generate certificate for gRPC server:
```bash
$ openssl req -new -nodes -keyout grpc.key -out grpc.csr -config ./openssl.cnf -subj "/C=PL/ST=mazowieckie/L=Warsaw/O=MiniStockMarket/OU=ministockmarket.pl/CN=ministockmarket.pl"
```

Sign certificate:
```bash
$ openssl x509 -req -days 3650 -in grpc.csr -signkey grpc.key -out grpc.crt -extensions v3_req -extfile ./openssl.cnf
```

Create secret:
```bash
$ kubectl create secret tls grpc --key grpc.key --cert grpc.crt -n ministockmarket
```